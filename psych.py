#!/usr/bin/env python
# read abelectronics ADC Pi V2 board inputs with repeating reading from each channel on a BeagleBone Black.
# Requires SMBus 

# Version 1.0  - 24/07/2013
# Version History:
# 1.0 - Initial Release

#
# Usage: changechannel(address, hexvalue) to change to new channel on adc chips
# Usage: getadcreading(address, hexvalue) to return value in volts from selected channel.
#
# address = adc_address1 or adc_address2 - Hex address of I2C chips as configured by board header pins.

from smbus import SMBus
import time
from time import sleep

import sys
import os
import BaseHTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler
import tornado.ioloop
import tornado.web
import tornado.websocket
import threading
msg='tmp'
adc_address1 = 0x68
adc_address2 = 0x69

#18 bit gain x1
channel1=0x9C
channel2=0xBC
channel3=0xDC
channel4=0xFC
varDivisior = 64 # from pdf sheet on adc addresses and config

adcreading = bytearray()

adcreading.append(0x00)
adcreading.append(0x00)
adcreading.append(0x00)
adcreading.append(0x00)

varMultiplier = (2.4705882/varDivisior)/1000

i2c_bus = 1             

bus = SMBus(i2c_bus)

class server(threading.Thread):
    def __init__(self):
       threading.Thread.__init__(self)
       HandlerClass = SimpleHTTPRequestHandler
       ServerClass = BaseHTTPServer.HTTPServer
       Protocol = "HTTP/1.0"
       server_address=("", 80)
       HandlerClass.protocol_version=Protocol
       self.httpd=ServerClass(server_address, HandlerClass)
       self.sa = self.httpd.socket.getsockname()

    def run(self):
        print "Serving HTTP on", self.sa[0], "port", self.sa[1],"..."
        self.httpd.serve_forever()

class WebSocket(tornado.websocket.WebSocketHandler):
    def open(self, *args):
        print 'socket opened'
        self.write_message("Connected")
        self.t=DaQ_write(self)
        self.t.start()

    def on_message(self, message):
        self.write_message(message)

    def on_close(self):
        self.t.stop()
        self.t.join()
        print 'socket closed'

class DaQ_write(threading.Thread):
    def __init__(self, socket):
        threading.Thread.__init__(self)
        self.socket=socket
        self.loop=True
        self.pause=1

    def run(self):
        global msg
        while self.loop:
            self.socket.write_message(msg)
            sleep(self.pause)

    def stop(self):
        self.loop=False


class DaQ(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.loop=True
        self.pause=1
        
    def run(self):
        global msg
        #write_old=""
        while self.loop:
            self.changechannel(adc_address1, channel1)
            p_b=self.lin(self.getadcreading(adc_address1, channel1),0,5,600,1100)*0.0295301
            self.changechannel(adc_address1, channel2)
            t_w0=self.lin(self.getadcreading(adc_address1, channel2),0.88,4.4,20,120)
            self.changechannel(adc_address1, channel3)
            t_d0=self.lin(self.getadcreading(adc_address1, channel3),0.88,4.4,20,120)
            R=53.35
            p_e=(2.96*10**-4)*t_w0**2-(1.59*10**-2)*t_w0+0.41
            p_p=p_e-p_b*((t_d0-t_w0)/2700)
            p_0=(70.73*(p_b-0.378*p_p))/(R*(t_d0+459.67))

            st = time.asctime(time.localtime(time.time()))
            
            msg=st
            msg+="<table>"
            msg+="<tr><td>Barometric:</td><td>"+str(round(p_b,2))+"</td></tr>"
            msg+="<tr><td>Dry Bulb:</td><td>"+str(round(t_d0,2))+"</td></tr>"
            msg+="<tr><td>Wet Bulb:</td><td>"+str(round(t_w0,2))+"</td></tr>"
            msg+="<tr><td>Air Density:</td><td>"+str(round(p_0,4))+"</td></tr></table>"
            #msg=st+"<br>Barometric:\t"+str(round(p_b,2))+"<br>Dry Bulb:\t"+str(round(t_d0,2))+"<br>Wet Bulb:\t"+str(round(t_w0,2))+"<br>Air Density:\t"+str(round(p_0,4))
            #write_new="Barometric:\t"+str(round(p_b,2))+"\nDry Bulb:\t"+str(round(t_d0,2))+"\nWet Bulb:\t"+str(round(t_w0,2))+"\nAir Density:\t"+str(round(p_0,2))
            #if write_old is not write_new:
            #    print 'Updating file'+write_old+write_new
            #    write_old=write_new
            #    text_file = open("Psych.txt", "w")
            #    text_file.write(write_old)
            #    text_file.close()
            time.sleep(self.pause)


    def changechannel(self, address, adcConfig):
        tmp= bus.write_byte(address, adcConfig)
    
    def getadcreading(self, address, adcConfig):
        adcreading = bus.read_i2c_block_data(address,adcConfig)
        h = adcreading[0]
        m = adcreading[1]
        l = adcreading[2]
        s = adcreading[3]
        # wait for new data
        while (s & 128):
            adcreading = bus.read_i2c_block_data(address,adcConfig)
            h = adcreading[0]
            m = adcreading[1]
            l = adcreading[2]
            s = adcreading[3]
        
        # shift bits to product result
        t = ((h & 0b00000001) << 16) | (m << 8) | l
        # check if positive or negative number and invert if needed
        if (h > 128):
            t = ~(0x020000 - t)
        return t * varMultiplier
    
    def lin(self,val,x1,x2,y1,y2):
        return (y1-y2)*(val-x1)/(x1-x2)+y1

def main():
    print os.getpid()
    t1=server()
    t1.setDaemon(True)
    t1.start()
    t2=DaQ()
    t2.setDaemon(True)
    t2.start()
    application=tornado.web.Application([
        (r'/', WebSocket),
    ])
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
