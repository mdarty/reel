#!/usr/bin/env python
# read abelectronics ADC Pi V2 board inputs with repeating reading from each channel on a BeagleBone Black.
# Requires SMBus 

# Version 1.0  - 24/07/2013
# Version History:
# 1.0 - Initial Release

#
# Usage: changechannel(address, hexvalue) to change to new channel on adc chips
# Usage: getadcreading(address, hexvalue) to return value in volts from selected channel.
#
# address = adc_address1 or adc_address2 - Hex address of I2C chips as configured by board header pins.

from smbus import SMBus
import time
from time import sleep

import sys
import os
import tornado.ioloop
import tornado.web
import tornado.websocket
import threading
import numpy
msg='tmp'
adc_address1 = 0x68
adc_address2 = 0x69

#18 bit gain x1
channel1=0x9C
channel2=0xBC
channel3=0xDC
channel4=0xFC
varDivisior = 64 # from pdf sheet on adc addresses and config

adcreading = bytearray()

adcreading.append(0x00)
adcreading.append(0x00)
adcreading.append(0x00)
adcreading.append(0x00)

varMultiplier = (2.4705882/varDivisior)/1000

i2c_bus = 1             

bus = SMBus(i2c_bus)

class WebSocket(tornado.websocket.WebSocketHandler):
    def open(self, *args):
        print 'socket opened'
        self.write_message("Connected")
        self.t=DaQ_write(self)
        self.t.start()

    def on_message(self, message):
        self.write_message(message)

    def on_close(self):
        self.t.stop()
        self.t.join()
        print 'socket closed'

class DaQ_write(threading.Thread):
    def __init__(self, socket):
        threading.Thread.__init__(self)
        self.socket=socket
        self.loop=True
        self.pause=1

    def run(self):
        global msg
        while self.loop:
            self.socket.write_message(msg)
            sleep(self.pause)

    def stop(self):
        self.loop=False


class DaQ(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.loop=True
        self.pause=0.05
	self.length=100
        
    def run(self):
        global msg
        #write_old=""
        self.changechannel(adc_address1, channel1)
        rpm=self.lin(self.getadcreading(adc_address1, channel1),0.88,4.4,1000,5000)
	arr=numpy.array([rpm])
        while self.loop:
            rpm=self.lin(self.getadcreading(adc_address1, channel1),0.88,4.4,1000,5000)
	    arr=numpy.append(arr,[rpm])
	    if len(arr)>self.length:
		arr=numpy.delete(arr, 0)
	    msg='RPM:'+str(round(rpm,2))+'\tMean:'+str(round(numpy.mean(arr),2))+'\tStd:'+str(round(numpy.std(arr),2))
            time.sleep(self.pause)


    def changechannel(self, address, adcConfig):
        tmp= bus.write_byte(address, adcConfig)
    
    def getadcreading(self, address, adcConfig):
        adcreading = bus.read_i2c_block_data(address,adcConfig)
        h = adcreading[0]
        m = adcreading[1]
        l = adcreading[2]
        s = adcreading[3]
        # wait for new data
        while (s & 128):
            adcreading = bus.read_i2c_block_data(address,adcConfig)
            h = adcreading[0]
            m = adcreading[1]
            l = adcreading[2]
            s = adcreading[3]
        
        # shift bits to product result
        t = ((h & 0b00000001) << 16) | (m << 8) | l
        # check if positive or negative number and invert if needed
        if (h > 128):
            t = ~(0x020000 - t)
        return t * varMultiplier
    
    def lin(self,val,x1,x2,y1,y2):
        return (y1-y2)*(val-x1)/(x1-x2)+y1

def main():
    print os.getpid()
    t1=DaQ()
    t1.setDaemon(True)
    t1.start()
    application=tornado.web.Application([
        (r'/', WebSocket),
    ])
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
